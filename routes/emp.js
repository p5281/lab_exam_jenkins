const { request, response } = require('express')
const express=require('express')
const router=express.Router()
const db=require('../db')
const utils=require('../utils')

router.get('/all',(request,response)=>{
    const connection=db.openConnection()
    const statement=`select * from emp`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})  

router.post('/add',(request,response)=>{
    const connection=db.openConnection()
    const{empid,name,salary,age}=request.body
    const statement=`insert into emp (empid, name, salary, age) 
    values(${empid},'${name}',${salary},${age})`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})
router.delete('/delete',(request,response)=>{
    const connection=db.openConnection()
    const{empid}=request.body
    const statement=`delete from emp where empid=${empid}`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})
router.patch('/update',(request,response)=>{
    const connection=db.openConnection()
    const{empid,salary}=request.body
    const statement=`update emp set salary=${salary} where empid=${empid}`
    connection.query(statement,(error,data)=>{
        connection.end()
        if(error){
            response.send(utils.createResult(error))
        }
        else{
            response.send(utils.createResult(error,data))
        }
    })

})
module.exports=router;